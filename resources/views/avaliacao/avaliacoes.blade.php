@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10 margin-tb">
            <div class="pull-left" style="margin-left: 10px">
                <h2>Avaliações Anteriores</h2>
            </div>
        </br>
        </div>
    </div>

    </br>
    <table class="table table-bordered">
        <tr>
            <th>Data</th>
            <th>Descrição</th>
            <th>Notas</th>
            <th></th>
        </tr>
	    @foreach ($avaliacao as $avaliacao)
	    <tr>
  	        <td>{{ $avaliacao['data'] }}</td>
            <td>{{ $avaliacao['descricao'] }}</td>
            <td>{{ $avaliacao['nota_arroz'] }}, {{ $avaliacao['nota_feijao'] }}, {{ $avaliacao['nota_carne'] }}, {{ $avaliacao['nota_suco'] }}.</td>
	          <td>
              <a class="btn btn-primary" href="{{ route('avaliacao.edit', $avaliacao->id) }}">Editar</a>
              @csrf
	          </td>
	    </tr>
	    @endforeach
    </table>

    <a class="btn btn-secondary" href="{{ route('home') }}">Voltar</a>
</div>
@endsection
