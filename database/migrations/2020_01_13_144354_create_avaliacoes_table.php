<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvaliacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacoes', function (Blueprint $table) {
            $table->string('id_aluno');
            $table->string('descricao');
            $table->datetime('data');
            $table->boolean('pendente');
            $table->integer('nota_arroz');
            $table->integer('nota_feijao');
            $table->integer('nota_carne');
            $table->integer('nota_suco');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliacaos');
    }
}
