@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Adicionar novo alimento</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('alimentos.index') }}">Back</a>
            </div>
        </div>
    </div>

    <form action="{{ route('alimentos.store') }}" method="POST">
    	@csrf
		       <strong>Tipo:</strong>
            <select class="form-control" name="tipo">
	             <option>Arroz</option>
	             <option>Feijão</option>
               <option>Carne</option>
	             <option>Suco</option>
	          </select>
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Nome:</strong>
		            <textarea class="form-control" style="height:150px" name="nome" placeholder="Nome do Alimento"></textarea>
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		            <button type="submit" class="btn btn-primary">Salvar</button>
		    </div>

   </form>
</div>
@endsection
