@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Alimento</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('alimentos.index') }}">Voltar</a>
            </div>
        </div>
    </div>

    </br>
    <form action="{{ route('alimentos.update',$alimento->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
        		  <div class="col-xs-12 col-sm-12 col-md-12">
        		       <div class="form-group">
        		           <strong>Tipo:</strong>
                       <select class="form-control" name="tipo">
                         <option> {{ $alimento->tipo }} </option>
                        @if($alimento->tipo != "Arroz")
           	             <option>Arroz</option>
                        @endif
                        @if($alimento->tipo != "Feijão")
           	             <option>Feijão</option>
                        @endif
                        @if($alimento->tipo != "Carne")
                         <option>Carne</option>
                        @endif
                        @if($alimento->tipo != "Suco")
           	             <option>Suco</option>
                        @endif
           	          </select>
        		       </div>
        		   </div>
        		  <div class="col-xs-12 col-sm-12 col-md-12">
        		       <div class="form-group">
        		           <strong>Nome:</strong>
        		           <textarea class="form-control" style="height:150px" name="nome">{{ $alimento->nome }}</textarea>
        		       </div>
        		  </div>
        		   <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        		     <button type="submit" class="btn btn-primary">Salvar</button>
        		   </div>
        	</div>
   </form>
</div>
@endsection
