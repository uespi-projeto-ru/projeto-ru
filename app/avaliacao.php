<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class avaliacao extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'avaliacoes';

    protected $fillable = ['id_aluno', 'data', 'descricao', 'pendente',
    'nota_arroz', 'nota_feijao', 'nota_carne', 'nota_suco'];
}
