@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10 margin-tb">
            <div class="pull-left" style="margin-left: 10px">
                <h2>Avaliações</h2>
            </div>
        </br>
        <div class="card-body">
             <a class="btn btn-primary"  href="{{ route('avaliacao.index') }}">Médias</a>
             <a class="btn btn-primary"  href="{{ route('avaliacao.index_total') }}">Lista Completa</a>
          </br>
        </div>
    </div>

    </br>
    <table class="table table-bordered">
        <tr>
            <th>Data</th>
            <th>Descrição</th>
            @if(array_key_exists('quantidade', $avaliacao[0]))
            <th>Quantidade</th>
            <th>Notas Médias</th>
            @else
            <th>Notas</th>
            @endif

        </tr>
	    @foreach ($avaliacao as $avaliacao)
	    <tr>
  	        <td>{{ $avaliacao['data'] }}</td>
            <td>{{ $avaliacao['descricao'] }}</td>
            @if(array_key_exists('quantidade', $avaliacao))
            <td>{{ $avaliacao['quantidade'] }}</td>
            @endif
            <td>{{ $avaliacao['nota_arroz'] }}, {{ $avaliacao['nota_feijao'] }}, {{ $avaliacao['nota_carne'] }}, {{ $avaliacao['nota_suco'] }}.</td>
	    </tr>
	    @endforeach
    </table>

    <a class="btn btn-secondary" href="{{ route('admin.home') }}">Voltar</a>
</div>
@endsection
