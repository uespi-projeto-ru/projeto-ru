@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add Novo Horario</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('horarios.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <form action="{{ route('horarios.store') }}" method="POST">
    	@csrf
         <div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Inicio:</strong>
		            <input type="text" name="inicio" class="form-control" placeholder="inicio">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Fim:</strong>
		            <textarea class="form-control" style="height:150px" name="fim" placeholder="Fim"></textarea>
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		            <button type="submit" class="btn btn-primary">Submit</button>
		    </div>
		</div>
   </form>

@endsection
