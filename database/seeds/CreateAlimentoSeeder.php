<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateAlimentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::collection('alimentos')->delete();

       DB::collection('alimentos')->insert([
           'tipo' => 'Arroz',
           'nome' => 'Arroz Branco',
       ]);

       DB::collection('alimentos')->insert([
           'tipo' => 'Arroz',
           'nome' => 'Maria Isabel',
       ]);

       DB::collection('alimentos')->insert([
           'tipo' => 'Feijão',
           'nome' => 'Feijão Carioca',
       ]);

       DB::collection('alimentos')->insert([
           'tipo' => 'Feijão',
           'nome' => 'Feijão Preto',
       ]);

       DB::collection('alimentos')->insert([
           'tipo' => 'Feijão',
           'nome' => 'Feijão Verde',
       ]);

       DB::collection('alimentos')->insert([
           'tipo' => 'Carne',
           'nome' => 'Frango Assado',
       ]);

       DB::collection('alimentos')->insert([
           'tipo' => 'Carne',
           'nome' => 'Linguiça Toscana',
       ]);

       DB::collection('alimentos')->insert([
           'tipo' => 'Carne',
           'nome' => 'Peixe Cozido',
       ]);

       DB::collection('alimentos')->insert([
           'tipo' => 'Suco',
           'nome' => 'Suco de Açaí',
       ]);

       DB::collection('alimentos')->insert([
           'tipo' => 'Suco',
           'nome' => 'Suco de Maracujá',
       ]);

       DB::collection('alimentos')->insert([
           'tipo' => 'Suco',
           'nome' => 'Suco de Laranja',
       ]);
    }
}
