<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ticket extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'ticket';

    protected $fillable = ['valor', 'estoque', 'limite_compra', 'limite_uso'];
}
