<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\cardapio;

class CreateAvaliacoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::collection('avaliacoes')->delete();
       $cardapios = cardapio::all();

       DB::collection('avaliacoes')->insert([
         'id_aluno' => (String) DB::collection('users')->where('is_admin', 0)->get()[0]['_id'],
         'data' => $cardapios[0]->data,
         'descricao' => $cardapios[0]->descricao,
         'pendente' => False,
         'nota_arroz' => 4,
         'nota_feijao' => 3,
         'nota_carne' => 4,
         'nota_suco' => 5,
       ]);

       DB::collection('avaliacoes')->insert([
         'id_aluno' => (String) DB::collection('users')->where('is_admin', 0)->get()[1]['_id'],
         'data' => $cardapios[0]->data,
         'descricao' => $cardapios[0]->descricao,
         'pendente' => False,
         'nota_arroz' => 5,
         'nota_feijao' => 3,
         'nota_carne' => 3,
         'nota_suco' => 3,
       ]);

       DB::collection('avaliacoes')->insert([
         'id_aluno' => (String) DB::collection('users')->where('is_admin', 0)->get()[2]['_id'],
         'data' => $cardapios[0]->data,
         'descricao' => $cardapios[0]->descricao,
         'pendente' => False,
         'nota_arroz' => 5,
         'nota_feijao' => 2,
         'nota_carne' => 2,
         'nota_suco' => 4,
       ]);

       DB::collection('avaliacoes')->insert([
         'id_aluno' => (String) DB::collection('users')->where('is_admin', 0)->get()[0]['_id'],
         'data' => $cardapios[1]->data,
         'descricao' => $cardapios[1]->descricao,
         'pendente' => False,
         'nota_arroz' => 3,
         'nota_feijao' => 3,
         'nota_carne' => 5,
         'nota_suco' => 3,
       ]);

       DB::collection('avaliacoes')->insert([
         'id_aluno' => (String) DB::collection('users')->where('is_admin', 0)->get()[1]['_id'],
         'data' => $cardapios[1]->data,
         'descricao' => $cardapios[1]->descricao,
         'pendente' => False,
         'nota_arroz' => 4,
         'nota_feijao' => 4,
         'nota_carne' => 1,
         'nota_suco' => 5,
       ]);
    }
}
