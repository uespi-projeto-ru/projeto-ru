@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10 margin-tb">
            <div class="pull-left" style="margin-left: 10px">
                <h2>Ticket</h2>
            </div>
        </br>
        </div>
    </div>


    </br>
    <table class="table table-bordered">
        <tr>
            <th>Valor</th>
            <th>Estoque</th>
            <th>Limite de Compra</th>
            <th>Limite de Uso Diário</th>
            <th width="280px">Action</th>
        </tr>
	    @foreach ($ticket as $ticket)
	    <tr>
	        <td>R${{ number_format($ticket->valor, 2, '.', '') }}</td>
            <td>{{ $ticket->estoque }}</td>
            <td>{{ $ticket->limite_compra }}</td>
            <td>{{ $ticket->limite_uso }}</td>
	        <td>
              @if(Auth::user()->is_admin)
                <form action="{{ route('ticket.destroy', $ticket->id) }}" method="POST">
                    <a class="btn btn-primary" href="{{ route('ticket.edit', $ticket->id) }}">Editar</a>
                    @csrf
                </form>
              @endif
	        </td>
	    </tr>
	    @endforeach
    </table>

    <a class="btn btn-secondary" href="{{ route('admin.home') }}">Voltar</a>
</div>
@endsection
