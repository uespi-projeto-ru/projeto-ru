@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10 margin-tb">
            <div class="pull-left" style="margin-left: 10px">
                <h2>Seus Pedidos</h2>
            </div>
        </br>
        </div>
    </div>
    </br>
    <table class="table table-bordered">
      <tr>
        <th>Descrição</th>
        <th>Valor</th>
        <th>Utilização</th>
      </tr>
    @foreach ($pedidos as $pedido)
    <tr>
        <td>
          @if($pedido['descricao_cardapio'] == NULL)

          @else
          {{ $pedido['descricao_cardapio'] }}
          @endif</td>
        <td>R${{ number_format($pedido['valor_ticket'], 2, '.', '') }}</td>
        <td>
          @if($pedido['data_de_utilizacao'])
            {{ $pedido['data_de_utilizacao'] }}
          @else
            Ticket não usado.</td>
          @endif
    </tr>
    @endforeach
    </table>
</div>
<div class="pull-right" style="margin-left: 128px">
  <a class="btn btn-secondary" href="{{ route('home') }}">Voltar</a>
</div>
@endsection
