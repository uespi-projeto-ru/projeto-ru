@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10 margin-tb">
            <div class="pull-left" style="margin-left: 11px">
                <h2>Cardápios</h2>
            </div>
        </br>
          <div class="pull-right">
              <a class="btn btn-success" style="margin-left: 11px" href="{{ route('cardapios.create') }}">Registrar um novo cardápio</a>
          </div>
        </div>
    </div>


    </br>
    <table class="table table-bordered">
        <tr>
            <th>Data</th>
            <th>Dia</th>
            <th>Descrição</th>
            <th width="280px">Ações</th>
        </tr>
	    @foreach ($cardapios as $cardapio)
	    <tr>
            <td>{{ $cardapio->data }}</td>
            <td>{{ $cardapio->dia }}</td>
            <td>{{ $cardapio->descricao }}</td>
	        <td>
            <form action="{{ route('cardapios.destroy',$cardapio->id) }}" method="POST">
                <a class="btn btn-primary" href="{{ route('cardapios.edit',$cardapio->id) }}">Editar</a>
                @method('DELETE')
                <button class="btn btn-danger" type="submit">Deletar</button>
                @csrf
            </form>
	        </td>
	    </tr>
	    @endforeach
    </table>

    <a class="btn btn-secondary" href="{{ route('admin.home') }}">Voltar</a>
</div>
@endsection
