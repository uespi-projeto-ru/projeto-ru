<?php

use Illuminate\Database\Seeder;

class CreateTicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::collection('ticket')->insert([
            'valor' => 2.50,
            'estoque' => 20,
            'limite_compra' => 5,
            'limite_uso' => 3,
        ]);
    }
}
