@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Adicionar novo cardápio</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('cardapios.index') }}">Voltar</a>
            </div>
        </div>
    </div>

    <form action="{{ route('cardapios.store') }}" method="POST">
    	@csrf
       <div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Data:</strong>
		            <input type="text" name="data" class="form-control" placeholder="Inicio">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Arroz:</strong>
                <select class="form-control" name="arroz">
                  @foreach ($arrozes as $arroz)
	                 <option>{{ $arroz['nome'] }}</option>
                  @endforeach
	              </select>
            </div>
            <div class="form-group">
                <strong>Feijão:</strong>
                <select class="form-control" name="feijao">
                  @foreach ($feijoes as $feijao)
	                 <option>{{ $feijao['nome'] }}</option>
                  @endforeach
	              </select>
            </div>
            <div class="form-group">
		            <strong>Carne:</strong>
                <select class="form-control" name="carne">
                  @foreach ($carnes as $carne)
	                 <option>{{ $carne['nome'] }}</option>
                  @endforeach
	              </select>
            </div>
            <div class="form-group">
                <strong>Suco:</strong>
                <select class="form-control" name="suco">
                  @foreach ($sucos as $suco)
	                 <option>{{ $suco['nome'] }}</option>
                  @endforeach
	              </select>
            </div>
		    </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Complemento Opcional:</strong>
		            <input type="text" name="complemento" class="form-control" placeholder="Complemento">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		            <button type="submit" class="btn btn-primary">Salvar</button>
		    </div>
		</div>

   </form>
</div>
@endsection
