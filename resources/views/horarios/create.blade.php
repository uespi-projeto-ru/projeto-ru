@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add Novo Horario</h2>
            </div>
            <div class="pull-right" style="height: 60px">
                <a class="btn btn-primary" href="{{ route('horarios.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <form action="{{ route('horarios.store') }}" method="POST">
    	@csrf
       <div class="row">
        		  <div class="col-xs-12 col-sm-12 col-md-12" style="height: 0px">
        		       <div class="form-group">
        		           <strong>Data de Início:</strong>
        		           <input type="text" style="height:40px; width: 90px" maxlength="5" name="inicioData" class="form-control">
        		       </div>
        		   </div>
        		  <div class="col-xs-12 col-sm-12 col-md-12" style="margin-left: 120px">
        		       <div class="form-group">
        		           <strong>Data Fim:</strong>
                           <input type="text" style="height:40px; width: 90px" maxlength="5" name="fimData" class="form-control">
        		       </div>
        		  </div>
              <div class="col-xs-12 col-sm-12 col-md-12" style="height: 0px">
        		     <div class="form-group">
        		           <strong>Hora de Início:</strong>
        		           <input type="text" style="height:40px; width: 90px" maxlength="5" name="inicioHora" class="form-control">
        		     </div>
        		   </div>
        		   <div class="col-xs-12 col-sm-12 col-md-12" style="margin-left: 120px">
        		     <div class="form-group">
        		           <strong>Hora Fim:</strong>
                           <input type="text" style="height:40px; width: 90px" maxlength="5" name="fimHora" class="form-control">
        		     </div>
        		   </div>
        		   <div class="col-xs-12 col-sm-12 col-md-12">
        		     <button type="submit" class="btn btn-primary">Salvar</button>
        		   </div>
        	</div>

   </form>
</div>
@endsection
