@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10 margin-tb">
            <div class="pull-left" style="margin-left: 10px">
                <h2>Alimentos</h2>
            </div>
        </br>
          <div class="pull-right">
              <a class="btn btn-success" style="margin-left: 10px" href="{{ route('alimentos.create') }}">Registrar um novo alimento</a>
          </div>
        </div>
    </div>


    </br>
    <table class="table table-bordered">
        <tr>
            <th>Tipo</th>
            <th>Nome</th>
            <th width="280px">Ações</th>
        </tr>
	    @foreach ($alimentos as $alimento)
	    <tr>
            <td>{{ $alimento->tipo }}</td>
            <td>{{ $alimento->nome }}</td>
	        <td>
            <form action="{{ route('alimentos.destroy',$alimento->id) }}" method="POST">
                <a class="btn btn-primary" href="{{ route('alimentos.edit',$alimento->id) }}">Editar</a>
                @method('DELETE')
                <button class="btn btn-danger" type="submit">Deletar</button>
                @csrf
            </form>
	        </td>
	    </tr>
	    @endforeach
    </table>

    <a class="btn btn-secondary" href="{{ route('admin.home') }}">Voltar</a>
</div>
@endsection
