<?php

use Illuminate\Database\Seeder;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::collection('users')->insert([
            'name' => 'João',
            'matricula' => '10',
            'password' => Hash::make('12345'),
            'is_admin' => 0,
            'saldo' => 20,00
        ]);

        DB::collection('users')->insert([
            'name' => 'Maria',
            'matricula' => '11',
            'password' => Hash::make('12345'),
            'is_admin' => 0,
            'saldo' => 50,00
        ]);

        DB::collection('users')->insert([
            'name' => 'Pedro',
            'matricula' => '12',
            'password' => Hash::make('12345'),
            'is_admin' => 0,
            'saldo' => 2500,00
        ]);
    }
}
