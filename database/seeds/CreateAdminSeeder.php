<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::collection('users')->delete();

       DB::collection('users')->insert([
           'name' => 'Prad',
           'matricula' => '1',
           'password' => Hash::make('12345'),
           'is_admin' => 1,
       ]);
    }
}
