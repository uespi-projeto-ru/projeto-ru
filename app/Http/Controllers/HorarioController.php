<?php

namespace App\Http\Controllers;

use App\horario;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $horarios = collect(horario::all())->sortBy(function($horario, $key){
        return Carbon::createFromFormat('d/m', $horario->inicioData);
      });
      return view('horarios.index',compact('horarios'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function edit(horario $horario)
    {
        return view('horarios.edit',compact('horario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, horario $horario)
    {
      request()->validate([
        'inicioData' => 'required|date_format:d/m',
        'fimData' => 'required|date_format:d/m',
        'inicioHora' => 'required|date_format:H:i',
        'fimHora' => 'required|date_format:H:i',
      ]);

      if($this->data_legitima($request)){
        $horario->update($request->all());

        return redirect()->route('horarios.index')->with('success','Horario atualizado com sucesso');
      }
      else {
        return redirect()->route('horarios.index')->with('error', 'Horário em conflito com outro.');
      }
    }

    public function create()
    {
        return view('horarios.create');
    }

    public function store(Request $request)
    {
        request()->validate([
            'inicioData' => 'required|date_format:d/m',
            'fimData' => 'required|date_format:d/m',
            'inicioHora' => 'required|date_format:H:i',
            'fimHora' => 'required|date_format:H:i',
        ]);

        if($this->data_legitima($request)){
          //dd($request);
          horario::create($request->all());

          return redirect()->route('horarios.index')
                          ->with('success','Horário criado com sucesso!');
        }
        else {
          return redirect()->route('horarios.index')->with('error', 'Horário em conflito com outro.');
        }
    }

    public function destroy(horario $horario)
    {
        $horario->delete();

        return redirect()->route('horarios.index')
                        ->with('success','Horário deletado com sucesso!');
    }

    public function data_legitima(Request $request){
        $horario = $request;
        $horario_novo_inicio = Carbon::createFromFormat('d/m', ($horario->inicioData));
        $horario_novo_fim = Carbon::createFromFormat('d/m', ($horario->fimData));
        $horarios_atuais = DB::table('horarios')->where('inicioData', '!=', $horario->inicioData)->where('fimData', '!=', $horario->fimData)->get();

        $validade = TRUE;

        foreach ($horarios_atuais as $horario_atual){
          $horario_atual_inicio = Carbon::createFromFormat('d/m', $horario_atual['inicioData']);
          $horario_atual_fim = Carbon::createFromFormat('d/m', $horario_atual['fimData']);

          if($horario_novo_inicio->between($horario_atual_inicio, $horario_atual_fim)
             or $horario_novo_fim->between($horario_atual_inicio, $horario_atual_fim)){
               $validade = FALSE;
          }
        }

        return $validade;
    }
}
