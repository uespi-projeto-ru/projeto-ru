@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Suas Avaliações</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('aluno.avaliacoes') }}"> Voltar</a>
            </div>
        </div>
    </div>

    </br>
    <form action="{{ route('avaliacao.update',$avaliacao->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
        		  <div class="col-xs-12 col-sm-12 col-md-12">
        		       <div class="form-group">
        		           <strong>Data:</strong> {{ $avaliacao->data }}
        		       </div>
        		   </div>
               <div class="col-xs-12 col-sm-12 col-md-12">
         		       <div class="form-group">
         		           <strong>Descrição:</strong> {{ $avaliacao->descricao }}
         		       </div>
         		   </div>
              <p><strong>Nota do Arroz:  </strong></p>
               <div class="col-xs-12 col-sm-12 col-md-12">
                <input type="radio" name="nota_arroz" value="1" {{ $avaliacao['nota_arroz'] == '1' ? 'checked' : ''}}> 1
                <input type="radio" name="nota_arroz" value="2" {{ $avaliacao['nota_arroz'] == '2' ? 'checked' : ''}}> 2
                <input type="radio" name="nota_arroz" value="3" {{ $avaliacao['nota_arroz'] == '3' ? 'checked' : ''}}> 3
                <input type="radio" name="nota_arroz" value="4" {{ $avaliacao['nota_arroz'] == '4' ? 'checked' : ''}}> 4
                <input type="radio" name="nota_arroz" value="5" {{ $avaliacao['nota_arroz'] == '5' ? 'checked' : ''}}> 5
               </div>
              <p><strong>Nota do Feijão:  </strong></p>
               <div class="col-xs-12 col-sm-12 col-md-12">
                 <input type="radio" name="nota_feijao" value="1" {{ $avaliacao['nota_feijao'] == '1' ? 'checked' : ''}}> 1
                 <input type="radio" name="nota_feijao" value="2" {{ $avaliacao['nota_feijao'] == '2' ? 'checked' : ''}}> 2
                 <input type="radio" name="nota_feijao" value="3" {{ $avaliacao['nota_feijao'] == '3' ? 'checked' : ''}}> 3
                 <input type="radio" name="nota_feijao" value="4" {{ $avaliacao['nota_feijao'] == '4' ? 'checked' : ''}}> 4
                 <input type="radio" name="nota_feijao" value="5" {{ $avaliacao['nota_feijao'] == '5' ? 'checked' : ''}}> 5
               </div>
              <p><strong>Nota da Carne:  </strong></p>
               <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="radio" name="nota_carne" value="1" {{ $avaliacao['nota_carne'] == '1' ? 'checked' : ''}}> 1
                  <input type="radio" name="nota_carne" value="2" {{ $avaliacao['nota_carne'] == '2' ? 'checked' : ''}}> 2
                  <input type="radio" name="nota_carne" value="3" {{ $avaliacao['nota_carne'] == '3' ? 'checked' : ''}}> 3
                  <input type="radio" name="nota_carne" value="4" {{ $avaliacao['nota_carne'] == '4' ? 'checked' : ''}}> 4
                  <input type="radio" name="nota_carne" value="5" {{ $avaliacao['nota_carne'] == '5' ? 'checked' : ''}}> 5
               </div>
              <p><strong>Nota do Suco:  </strong></p>
               <div class="col-xs-12 col-sm-12 col-md-12">
                  <input type="radio" name="nota_suco" value="1" {{ $avaliacao['nota_suco'] == '1' ? 'checked' : ''}}> 1
                  <input type="radio" name="nota_suco" value="2" {{ $avaliacao['nota_suco'] == '2' ? 'checked' : ''}}> 2
                  <input type="radio" name="nota_suco" value="3" {{ $avaliacao['nota_suco'] == '3' ? 'checked' : ''}}> 3
                  <input type="radio" name="nota_suco" value="4" {{ $avaliacao['nota_suco'] == '4' ? 'checked' : ''}}> 4
                  <input type="radio" name="nota_suco" value="5" {{ $avaliacao['nota_suco'] == '5' ? 'checked' : ''}}> 5
               </div>
        		   <div class="col-xs-12 col-sm-12 col-md-12 text-center pull-left">
        		     <button type="submit" class="btn btn-primary">Salvar</button>
        		   </div>
        	</div>
   </form>
@endsection
