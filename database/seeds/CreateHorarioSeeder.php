<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateHorarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::collection('horarios')->delete();

       DB::collection('horarios')->insert([
           'inicioData' => '01/07',
           'fimData' => '20/12',
           'inicioHora' => '11:00',
           'fimHora' => '14:00',
       ]);

       DB::collection('horarios')->insert([
           'inicioData' => '01/01',
           'fimData' => '30/06',
           'inicioHora' => '14:00',
           'fimHora' => '16:00',
       ]);
    }
}
