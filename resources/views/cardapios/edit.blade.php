@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Cardápio</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('cardapios.index') }}"> Voltar</a>
            </div>
        </div>
    </div>

    </br>
    <form action="{{ route('cardapios.update',$cardapio->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
        		  <div class="col-xs-12 col-sm-12 col-md-12">
        		       <div class="form-group">
        		           <strong>Data:</strong>
        		           <input type="text" name="data" value="{{ $cardapio->data }}" class="form-control" placeholder="Fim">
        		       </div>
        		   </div>
               <div class="col-xs-12 col-sm-12 col-md-12">
       		        <div class="form-group">
       		            <strong>Arroz:</strong>
                       <select name="arroz">
                         <option>{{ $arrozAtual }}</option>
                         @foreach ($arrozes as $arroz)
                          @if($arroz['nome'] != $arrozAtual)
       	                   <option>{{ $arroz['nome'] }}</option>
                          @endif
                         @endforeach
       	              </select>
                   </div>
                   <div class="form-group">
                       <strong>Feijão:</strong>
                       <select name="feijao">
                         <option>{{ $feijaoAtual }}</option>
                         @foreach ($feijoes as $feijao)
                          @if($feijao['nome'] != $feijaoAtual)
       	                   <option>{{ $feijao['nome'] }}</option>
                          @endif
                         @endforeach
       	              </select>
                   </div>
                   <div class="form-group">
        		            <strong>Carne:</strong>
                        <select name="carne">
                          <option>{{ $carneAtual }}</option>
                          @foreach ($carnes as $carne)
                           @if($carne['nome'] != $carneAtual)
        	                   <option>{{ $carne['nome'] }}</option>
                           @endif
                          @endforeach
        	              </select>
                    </div>
                   <div class="form-group">
                       <strong>Suco:</strong>
                       <select name="suco">
                         <option>{{ $sucoAtual }}</option>
                         @foreach ($sucos as $suco)
                          @if($suco['nome'] != $sucoAtual)
       	                   <option>{{ $suco['nome'] }}</option>
                          @endif
                         @endforeach
       	              </select>
                   </div>
       		    </div>
               <div class="col-xs-12 col-sm-12 col-md-12">
       		        <div class="form-group">
       		            <strong>Complemento Opcional:</strong>
       		            <input type="text" name="complemento" class="form-control" placeholder="Complemento" value="{{ $complemento }}">
       		        </div>
       		    </div>
        		   <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        		     <button type="submit" class="btn btn-primary">Salvar</button>
        		   </div>
        	</div>
   </form>
</div>
@endsection
