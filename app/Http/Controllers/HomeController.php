<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\ticket;
use App\horario;
use App\cardapio;
use App\avaliacao;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ticket = ticket::first();
        $ticketValor = number_format($ticket->valor, 2);
        $pedidos = DB::collection('pedidos')->where('user_id', Auth::id())->get()->sortBy('data_de_utilizacao')->take($ticket->first()->limite_compra + 3);
        $horarios = horario::all();
        $cardapios = cardapio::all();
        $avaliacoes_pendentes = avaliacao::where('pendente', True)->get();
        $cardapioDeHoje = NULL;
        
        foreach ($cardapios as $cardapio){
          if($cardapio->data == Carbon::now()->format('d/m/Y')){
            $cardapioDeHoje = $cardapio->descricao;
          }
        }

        foreach ($horarios as $horario){
          if(Carbon::createFromFormat('d/m', $horario->inicioData)->isPast() and Carbon::createFromFormat('d/m', $horario->fimData)->isFuture()){
            $horario_funcionamento = $horario;
          }
        }

        return view('home', ['pedidos' => $pedidos, 'ticket' => $ticket, 'ticketValor' => $ticketValor,
        'horario_funcionamento' => $horario_funcionamento, 'cardapio' => $cardapioDeHoje,
        'avaliacoes_pendentes' => $avaliacoes_pendentes]);
    }

    public function adminHome()
    {
      $horarios = horario::all();

      foreach ($horarios as $horario){
        if(Carbon::createFromFormat('d/m', $horario->inicioData)->isPast() and Carbon::createFromFormat('d/m', $horario->fimData)->isFuture()){
          $horario_funcionamento = $horario;
        }
      }
      $semanaAtual = Carbon::now()->startOfWeek();
      $arraySemana = ["segunda" => cardapio::all()->where('data', $semanaAtual->format('d/m/Y'))->first()->descricao,
      "terca" => cardapio::all()->where('data', $semanaAtual->addDay(1)->format('d/m/Y'))->first()->descricao,
      "quarta" => cardapio::all()->where('data', $semanaAtual->addDay(1)->format('d/m/Y'))->first()->descricao,
      "quinta" => cardapio::all()->where('data', $semanaAtual->addDay(1)->format('d/m/Y'))->first()->descricao,
      "sexta" => cardapio::all()->where('data', $semanaAtual->addDay(1)->format('d/m/Y'))->first()->descricao,
      "sabado" => cardapio::all()->where('data', $semanaAtual->addDay(1)->format('d/m/Y'))->first()->descricao,
      "domingo" => cardapio::all()->where('data', $semanaAtual->addDay(1)->format('d/m/Y'))->first()->descricao];

      return view('adminHome', ['horario' => $horario_funcionamento, 'cardapio_semanal' => $arraySemana]);
    }

    public function relatorioCompras(){
      $compras = DB::collection('pedidos')->get();
      $qtd_datas = DB::collection('pedidos')->select('data_compra')->count();

      $data_e_valor[] = null;

      for ($i = 0; $i < count($compras); $i++){

        if(array_key_exists($compras[$i]["data_compra"], $data_e_valor)){

          $aux = $data_e_valor[$compras[$i]["data_compra"]][0] + $compras[$i]["valor_ticket"];
          $aux2 = $data_e_valor[$compras[$i]["data_compra"]][1] + 1;

          $data_e_valor[$compras[$i]["data_compra"]] = [$aux, $aux2];
        } else {
          $data_e_valor[$compras[$i]["data_compra"]] = [$compras[$i]["valor_ticket"], 1];
        }

      }
      $datas = array_keys($data_e_valor);

      $relatorios[] = array();

      for ($i = 0; $i < count($data_e_valor); $i++){
        $relatorios[$i]['datas'] = $datas[$i];
        $relatorios[$i]['dinheiros'] = $data_e_valor[$datas[$i]][0];
        $relatorios[$i]['qtdCompras'] = $data_e_valor[$datas[$i]][1];
      }

      array_shift($relatorios);

      return view('relatorios.relatorio', ['relatorios' => $relatorios]);
    }
}
