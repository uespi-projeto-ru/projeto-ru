@extends('layouts.app')

@section('content')
<div class="container">
  <center>
    <div>
        <div class="col-md-8">
            <div class="card"  style="width:100%; height: 100%">
                <div class="card-header" style="margin-left: 183px; height: 60px">Página do Administrador</div>
                <div class="card-body">
                     <a class="btn btn-primary"  href="{{ route('ticket.index') }}">Ticket</a>
                     <a class="btn btn-primary"  href="{{ route('cardapios.index') }}">Cardapios</a>
                     <a class="btn btn-primary"  href="{{ route('alimentos.index') }}">Alimentos</a>
                     <a class="btn btn-primary"  href="{{ route('horarios.index') }}">Horários</a>
                     <a class="btn btn-primary"  href="{{ route('relatorios') }}">Relatorios</a>
                     <a class="btn btn-primary"  href="{{ route('avaliacao.index') }}">Avaliações</a>
                  </br>
                <div style="height: 30px; margin-top: 10px">
                Bem vindo {{ Auth::user()->name }}.
                @if($horario)
                  <br>Horário de funcionamento atual: {{ $horario->inicioHora }} às {{ $horario->fimHora }}.
                @else
                  <br>Nenhum horário de funcionamento configurado.
                @endif
                </div>
                <br><strong>Menu Semanal:</strong>
                <table class="table table-bordered">
                    <tr>
                        <th>Dia</th>
                        <th>Cardápio</th>
                    </tr>
            	    <tr>
                        <td><strong>Segunda-Feira</strong></td>
                        <td>{{ $cardapio_semanal['segunda'] }}</td>
            	    </tr>
                  <tr>
                        <td><strong>Terça-Feira</strong></td>
                        <td>{{ $cardapio_semanal['terca'] }}</td>
            	    </tr>
                  <tr>
                        <td><strong>Quarta-Feira</strong></td>
                        <td>{{ $cardapio_semanal['quarta'] }}</td>
            	    </tr>
                  <tr>
                        <td><strong>Quinta-Feira</strong></td>
                        <td>{{ $cardapio_semanal['quinta'] }}</td>
            	    </tr>
                  <tr>
                        <td><strong>Sexta-Feira</strong></td>
                        <td>{{ $cardapio_semanal['sexta'] }}</td>
            	    </tr>
                  <tr>
                        <td><strong>Sábado</strong></td>
                        <td>{{ $cardapio_semanal['sabado'] }}</td>
            	    </tr>
                  <tr>
                        <td><strong>Domingo</strong></td>
                        <td>{{ $cardapio_semanal['domingo'] }}</td>
            	    </tr>
                </table>
                </div>
        </div>
    </div>
 </center>
</div>
@endsection
