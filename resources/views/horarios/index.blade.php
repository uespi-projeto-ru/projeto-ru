@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left" style="margin-left: 10px">
                <h2>Horário de Funcionamento</h2>
            </div>
            <div class="pull-right" style="height: 55px; margin-left: 10px">
                <a class="btn btn-success" href="{{ route('horarios.create') }}"> Criar um novo horário</a>
            </div>
        </div>
    </div>
    <div>
        <table class="table table-bordered">
            <tr>
                <th>Data de Início</th>
                <th>Data Fim</th>
                <th>Hora de Início</th>
                <th>Hora Fim</th>
                <th width="280px"></th>
            </tr>
	        @foreach ($horarios as $horario)
	        <tr>
	           <td>{{ $horario->inicioData }}</td>
	           <td>{{ $horario->fimData }}</td>
             <td>{{ $horario->inicioHora }}</td>
	           <td>{{ $horario->fimHora }}</td>
	           <td>
                <form action="{{ route('horarios.destroy',$horario->id) }}" method="POST">
                    <a class="btn btn-primary" href="{{ route('horarios.edit',$horario->id) }}">Editar</a>
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">Deletar</button>
                    @csrf
                </form>
	           </td>
	        </tr>
	        @endforeach
        </table>
    </div>
    <a class="btn btn-secondary" href="{{ route('admin.home') }}">Voltar</a>
</div>
@endsection
