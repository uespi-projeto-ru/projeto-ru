<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Pedido extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'pedidos';

    protected $fillable = ['user_id', 'ticket_id', 'data_compra', 'descricao_cardapio', 'valor_ticket', 'data_de_utilizacao'];
}
