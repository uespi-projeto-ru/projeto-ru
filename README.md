## Projeto-RU
Projeto desenvolvido por alunos do curso de Ciência da Computação na UESPI para a matéria Engenharia de Software II, com o objetivo de simular o desenvolvimento de um sistema com estrutura necessária para atender os requisitos do Restaurante Universitário da UESPI, além do aprendizado na prática do método de desenvolvimento Scrum.

## Desenvolvedores
[Anderson Monte](https://gitlab.com/andersonmonte)

[Giovanni Lucas](https://gitlab.com/giovannilucasmoura)

[Marcelo Segundo](https://gitlab.com/marcelo_segundo)

[Pedro Henrique Amorim](https://gitlab.com/phca8803)

[Vinícius Marques](https://gitlab.com/Vinicius_Marques)
