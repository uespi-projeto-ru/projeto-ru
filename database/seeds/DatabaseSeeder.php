<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CreateAdminSeeder::class);
        $this->call(CreateUserSeeder::class);
        $this->call(CreateHorarioSeeder::class);
        $this->call(CreateTicketSeeder::class);
        $this->call(CreateAlimentoSeeder::class);
        $this->call(CreateCardapioSeeder::class);
        $this->call(CreateAvaliacoesSeeder::class);
        $this->call(CreatePedidosSeeder::class);
    }
}
