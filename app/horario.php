<?php
namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class horario extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'horarios';


  protected $fillable = ['inicioData', 'fimData', 'inicioHora', 'fimHora'];
}
