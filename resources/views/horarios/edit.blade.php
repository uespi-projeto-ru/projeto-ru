@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Horario</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('horarios.index') }}">Voltar</a>
            </div>
        </div>
    </div>

    </br>
    <form action="{{ route('horarios.update',$horario->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
        		  <div class="col-xs-12 col-sm-12 col-md-12" style="height: 0px">
        		       <div class="form-group">
        		           <strong>Data de Início:</strong>
        		           <input type="text" style="height:40px; width: 90px" maxlength="5" name="inicioData" value="{{ $horario->inicioData }}" class="form-control" placeholder="Fim">
        		       </div>
        		   </div>
        		  <div class="col-xs-12 col-sm-12 col-md-12" style="margin-left: 120px">
        		       <div class="form-group">
        		           <strong>Data Fim:</strong>
                           <input type="text" style="height:40px; width: 90px" maxlength="5" name="fimData" value="{{ $horario->fimData }}" class="form-control">
        		       </div>
        		  </div>
              <div class="col-xs-12 col-sm-12 col-md-12" style="height: 0px">
        		     <div class="form-group">
        		           <strong>Hora de Início:</strong>
        		           <input type="text" style="height:40px; width: 90px" maxlength="5" name="inicioHora" value="{{ $horario->inicioHora }}" class="form-control" placeholder="Fim">
        		     </div>
        		   </div>
        		   <div class="col-xs-12 col-sm-12 col-md-12" style="margin-left: 120px">
        		     <div class="form-group">
        		           <strong>Hora Fim:</strong>
                           <input type="text" style="height:40px; width: 90px" maxlength="5" name="fimHora" value="{{ $horario->fimHora }}" class="form-control">
        		     </div>
        		   </div>
        		   <div class="col-xs-12 col-sm-12 col-md-12">
        		     <button type="submit" class="btn btn-primary">Salvar</button>
        		   </div>
        	</div>
   </form>
</div>
@endsection
