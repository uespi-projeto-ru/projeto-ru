<?php

namespace App\Http\Controllers;

use App\alimento;
use Illuminate\Http\Request;

class AlimentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $alimentos = alimento::all()->sortBy('tipo');

      return view('alimentos.index', ['alimentos' => $alimentos])
          ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('alimentos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
          'nome' => 'required',
          'tipo' => 'required',
      ]);

      alimento::create($request->all());

      return redirect()->route('alimentos.index')
                      ->with('success','Alimento registrado com sucesso.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(alimento $alimento)
    {
        return view('alimentos.edit',compact('alimento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, alimento $alimento)
    {
      $request->validate([
          'tipo' => 'required',
          'nome' => 'required',
      ]);

      $alimento->update($request->all());

      return redirect()->route('alimentos.index')
                      ->with('success','Alimento atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(alimento $alimento)
    {
      $alimento->delete();

      return redirect()->route('alimentos.index')
                      ->with('success','Alimento removido com sucesso.');
    }
}
