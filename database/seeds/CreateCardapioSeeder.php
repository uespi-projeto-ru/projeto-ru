<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\alimento;

class CreateCardapioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::collection('cardapios')->delete();

       $arrayArroz = DB::collection('alimentos')->where('tipo', 'Arroz')->get();
       $arrayFeijao = DB::collection('alimentos')->where('tipo', 'Feijão')->get();
       $arrayCarne = DB::collection('alimentos')->where('tipo', 'Carne')->get();
       $arraySuco = DB::collection('alimentos')->where('tipo', 'Suco')->get();

       for($i = -1; $i <= 7; $i++){
         $arroz = $arrayArroz[rand(0, sizeof($arrayArroz) - 1)]['nome'];
         $feijao = $arrayFeijao[rand(0, sizeof($arrayFeijao) - 1)]['nome'];
         $carne = $arrayCarne[rand(0, sizeof($arrayCarne) - 1)]['nome'];
         $suco = $arraySuco[rand(0, sizeof($arraySuco) - 1)]['nome'];
         switch($i){
           case -1:
            $dia = "Sábado";
            break;
           case 0:
            $dia = "Domingo";
            break;
           case 1:
            $dia = "Segunda-Feira";
            break;
           case 2:
            $dia = "Terça-Feira";
            break;
           case 3:
            $dia = "Quarta-Feira";
            break;
           case 4:
            $dia = "Quinta-Feira";
            break;
           case 5:
            $dia = "Sexta-Feira";
            break;
           case 6:
            $dia = "Sábado";
            break;
           case 7:
            $dia = "Domingo";
            break;
         }

         DB::collection('cardapios')->insert([
             'dia' => $dia,
             'data' => Carbon::now()->startOfWeek()->subDays(1)->addDays($i)->format('d/m/Y'),
             'descricao' => $arroz . ', ' . $feijao . ', ' . $carne . ', ' . $suco . '.',
         ]);
       }
    }
}
