<?php

namespace App\Http\Controllers;

use App\cardapio;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CardapioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $cardapios = cardapio::all()->sortByDesc('_id');

      return view('cardapios.index', ['cardapios' => $cardapios])
          ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $arrozes = DB::collection('alimentos')->where('tipo', 'Arroz')->get();
      $feijoes = DB::collection('alimentos')->where('tipo', 'Feijão')->get();
      $carnes = DB::collection('alimentos')->where('tipo', 'Carne')->get();
      $sucos = DB::collection('alimentos')->where('tipo', 'Suco')->get();

      return view('cardapios.create', ['arrozes' => $arrozes, 'feijoes' => $feijoes, 'carnes' => $carnes, 'sucos' => $sucos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
          'data' => 'required|date_format:d/m/Y',
          'arroz' => 'required',
          'feijao' => 'required',
          'carne' => 'required',
          'suco' => 'required',
      ]);

      foreach (cardapio::all() as $cardapio){
        if($cardapio->data == $request->data){
          return redirect()->route('cardapios.index')
                          ->with('error','O cardápio desse dia já foi registrado.');
        }
      }
      $dataTraduzida = $this->traduzir_dia(Carbon::createFromFormat('d/m/Y', $request->data)->format('l'));

      if($request->complemento){
        $descricao = $request->arroz . ', ' . $request->feijao . ', ' . $request->carne . ', ' . $request->suco . ', ' . $request->complemento . '.';
      }
      else{
        $descricao = $request->arroz . ', ' . $request->feijao . ', ' . $request->carne . ', ' . $request->suco . '.';
      }
      cardapio::create(['data' => $request->data, 'dia' => $dataTraduzida, 'descricao' => $descricao]);

      return redirect()->route('cardapios.index')
                      ->with('success','Cardápio configurado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cardapio  $cardapio
     * @return \Illuminate\Http\Response
     */
    public function show(cardapio $cardapio)
    {
        return view('cardapios.show', compact('cardapio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cardapio  $cardapio
     * @return \Illuminate\Http\Response
     */
    public function edit(cardapio $cardapio)
    {
        $arrozes = DB::collection('alimentos')->where('tipo', 'Arroz')->get();
        $feijoes = DB::collection('alimentos')->where('tipo', 'Feijão')->get();
        $carnes = DB::collection('alimentos')->where('tipo', 'Carne')->get();
        $sucos = DB::collection('alimentos')->where('tipo', 'Suco')->get();

        $arrozAtual = explode(",", $cardapio->descricao)[0];
        $feijaoAtual = substr(explode(",", $cardapio->descricao)[1], 1);
        $carneAtual = substr(explode(",", $cardapio->descricao)[2], 1);
        if(count(explode(",", $cardapio->descricao)) == 5){
          $sucoAtual = substr(explode(",", $cardapio->descricao)[3], 1);
          $complemento = substr(explode(",", $cardapio->descricao)[4], 1, -1);
        }
        else{
          $sucoAtual = substr(explode(",", $cardapio->descricao)[3], 1, -1);
          $complemento = NULL;
        }

        return view('cardapios.edit', ['arrozes' => $arrozes, 'feijoes' => $feijoes, 'carnes' => $carnes, 'sucos' => $sucos,
        'arrozAtual' => $arrozAtual, 'feijaoAtual' => $feijaoAtual, 'carneAtual' => $carneAtual, 'sucoAtual' => $sucoAtual,
        'complemento' => $complemento, 'cardapio' => $cardapio]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cardapio  $cardapio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cardapio $cardapio)
    {
      $request->validate([
          'data' => 'required|date_format:d/m/Y',
          'arroz' => 'required',
          'feijao' => 'required',
          'carne' => 'required',
          'suco' => 'required',
      ]);

      foreach (cardapio::all()->where('_id', '<>', $cardapio->_id) as $cardapioForEach){
        if($cardapioForEach->data == $request->data){
          return redirect()->route('cardapios.index')
                          ->with('error','O cardápio desse dia já foi registrado.');
        }
      }

      $dataTraduzida = $this->traduzir_dia(Carbon::createFromFormat('d/m/Y', $request->data)->format('l'));

      if($request->complemento){
        $descricao = $request->arroz . ', ' . $request->feijao . ', ' . $request->carne . ', ' . $request->suco . ', ' . $request->complemento . '.';
      }
      else{
        $descricao = $request->arroz . ', ' . $request->feijao . ', ' . $request->carne . ', ' . $request->suco . '.';
      }

      $cardapio->update(['data' => $request->data, 'dia' => $dataTraduzida, 'descricao' => $descricao]);

      return redirect()->route('cardapios.index')->with('success','Cardápio atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cardapio  $cardapio
     * @return \Illuminate\Http\Response
     */
    public function destroy(cardapio $cardapio)
    {
      $cardapio->delete();

      return redirect()->route('cardapios.index')
                      ->with('success','Cardápio removido com sucesso.');
    }

    public function traduzir_dia(String $dia){
      switch($dia){
        case "Monday":
          $dataTraduzida = "Segunda-Feira";
          break;
        case "Tuesday":
          $dataTraduzida = "Terça-Feira";
          break;
        case "Wednesday":
          $dataTraduzida = "Quarta-Feira";
          break;
        case "Thursday":
          $dataTraduzida = "Quinta-Feira";
          break;
        case "Friday":
          $dataTraduzida = "Sexta-Feira";
          break;
        case "Saturday":
          $dataTraduzida = "Sábado";
          break;
        case "Sunday":
          $dataTraduzida = "Domingo";
          break;
        }

      return $dataTraduzida;
    }
}
