<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class cardapio extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'cardapios';

    protected $fillable = ['data', 'dia', 'descricao'];
}
