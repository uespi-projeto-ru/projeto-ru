<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class alimento extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'alimentos';

    protected $fillable = ['nome', 'tipo'];
}
