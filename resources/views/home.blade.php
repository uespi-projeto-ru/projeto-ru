@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card"  style="width:100%; height: 100%">
        <div>
            <div style="margin-left: 440px" class="card-header">Página Inicial</div>
            <table class="table table-bordered">
              <tr>
                <td>
                <div style="margin-left: 20px">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bem vindo {{ Auth::user()->name }}.

                    <form action="{{ route('alunoSaldoAdd') }}" method="POST">
                    @csrf
                        Seu saldo é R${{ number_format(Auth::user()->saldo, 2) }}
                        <input class="form-control" style="width: 120px; height:40px;"  name="saldoAdd" placeholder="Saldo"></input>
                      </br>
                        <button type="submit" class="btn btn-primary">Adicionar Saldo</button>
                      <br>
                    </form>
                </div>
                </td>
                <td>
                <div style="margin-right: 20px;margin-left: 20px ">
                      Custo do ticket atual: R${{ $ticketValor }}. </br> Limite de compras: {{ $ticket['limite_compra'] }}.

                      <form action="{{ route('comprar', $ticket) }}" method="get">
                      @csrf
                      <input type="number" id="quantidade" name="quantidade" min="1" width= "10px" placeholder="Quantidade">

                        @if ($ticket->estoque >= 1)
                          <button type="submit" class="btn btn-primary">Comprar</button>
                        @else
                          Ticket fora de estoque.
                        @endif
                      </form>
                </div>
                </td>
                <td>
                <div style="margin-top: -23px">
                        @if($horario_funcionamento)
                          <br>Horário de Funcionamento: {{ $horario_funcionamento->inicioHora }} às {{ $horario_funcionamento->fimHora }}.
                          <br>Limite de uso de ticket por dia: {{ $ticket['limite_uso'] }}.
                          <br>Cardápio de Hoje: {{ $cardapio }}</br>
                          <div class = "row-center">
                            <a class="btn btn-primary" href="{{ route('usar') }}">Usar Ticket</a>
                          </div>
                        @else
                          O Restaurante Universitário está fora de funcionamento.
                        @endif
                </div>
                </td>
              </tr>
            </table>
            <div style="margin-left: 350px" class="card-header">Últimas Compras
                <a class="btn btn-primary" href="{{ route('aluno.pedidos') }}">Mostrar Todas</a>
                </div>
                <table class="table table-bordered">
                  <tr>
                    <th>Descrição</th>
                    <th>Valor</th>
                    <th>Utilização</th>
                  </tr>
                @foreach ($pedidos as $pedido)
                <tr>
                    <td>
                      @if($pedido['descricao_cardapio'] == NULL)

                      @else
                      {{ $pedido['descricao_cardapio'] }}
                      @endif</td>
                    <td>R${{ number_format($pedido['valor_ticket'], 2, '.', '') }}</td>
                    <td>
                      @if($pedido['data_de_utilizacao'])
                        {{ $pedido['data_de_utilizacao'] }}
                      @else
                        Ticket não usado.</td>
                      @endif
                </tr>
                @endforeach
                </table>
    </div>

            <div style="margin-left: 330px" class="card-header">Avaliacões Pendentes
                <a class="btn btn-primary" href="{{ route('aluno.avaliacoes') }}">Mostrar Todas</a>
            </div>
            <table class="table table-bordered">
                <tr>
                    <th>Data</th>
                    <th>Descrição</th>
                    <th></th>
                </tr>
                @foreach ($avaliacoes_pendentes as $avaliacao)
                @csrf
                  <tr>
                      <td>{{ $avaliacao->data }}</td>
                      <td>{{ $avaliacao->descricao }}</td>
                      <td><a class="btn btn-primary" href="{{ route('avaliacao.edit', $avaliacao->id) }}">Avaliar</a></td>
                    </tr>
                @endforeach
            </table>
    </div>
</div>
@endsection
