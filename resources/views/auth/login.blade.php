@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            </br>
            <center>
            <div class="card">
                <div class="card-header">{{ __('Efetue Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Matricula:') }}</label>

                            <div class="col-md-6">
                                <input id="matricula" type="text" class="form-control @error('matricula') is-invalid @enderror" name="matricula" value="{{ old('matricula') }}" required autocomplete="matricula" autofocus>

                                @error('matricula:')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Senha') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Lembrar-me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0"  style="margin-right: 100px">
                            <div class="col-md-8 offset-md-2">
                                <button type="submit" class="btn btn-lg btn-primary btn-block">
                                    {{ __('Entrar') }}
                                </button>
                            </div>
                        </div>
                    </br>
                       <div><a href="recuperarconta" class="btn btn-sm btn-danger" style="margin-left: 10px">Recuperar Senha</a> <a href={{ route('giomar') }} class="btn btn-sm btn-danger" style="margin-left: 45px">Consultar matricula</a></div>

                    </form>
                </div>
            </div>
        </div>
        </center>
    </div>
</div>
<div class="clear"></div>
<footer>
            <div class="container footer">
              <!-- <div class="borda"></div> -->
              <h6 align="center">NPD - Núcleo de Processamento de dados. <br> Em caso de problemas entre em contato pelo Email alunonline@uespi.br<br> lembrando sempre de informar sua matrícula.<br>© 2015~2019 Universidade Estadual do Piauí - UESPI. Todos os direitos reservados.</h6>            </div>
          </footer>
@endsection
