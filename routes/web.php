<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\TicketController;
use App\ticket;
use App\horario;
use App\cardapio;
use App\avaliacao;
use Carbon\Carbon;

Auth::routes();

Route::get('/', function () {
  if(Auth::user()){
    if(auth()->user()->is_admin == 1){
      return redirect()->route('admin.home');
    }
    else {
      return redirect()->route('home');
    }
  }
  else{
    return view('/auth/login');
  }
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin'], function(){
  Route::resource('ticket', 'TicketController')->middleware('is_admin');
  Route::resource('horarios','HorarioController')->middleware('is_admin');
  Route::resource('cardapios','CardapioController')->middleware('is_admin');
  Route::resource('alimentos', 'AlimentoController')->middleware('is_admin');
  Route::resource('avaliacao', 'AvaliacaoController');
  Route::get('/avaliacoes/','AvaliacaoController@index_total')->name('avaliacao.index_total')->middleware('is_admin');
  Route::get('/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');
  Route::get('/relatorios', 'HomeController@relatorioCompras')->name('relatorios')->middleware('is_admin');
});

Route::group(['prefix'=>'aluno'], function(){
  Route::get('/usar_ticket', 'PedidoController@alunoUsarTicket')->name('usar');
  Route::post('/saldo_add', 'PedidoController@alunoSaldoAdd')->name('alunoSaldoAdd');
  Route::get('/comprar/{ticket}', 'PedidoController@compra')->name('comprar');
  Route::get('/avaliacoes','AvaliacaoController@indexAluno')->name('aluno.avaliacoes');
  Route::get('/pedidos','PedidoController@indexPedidos')->name('aluno.pedidos');
});















































// ?
Route::get('/giomar', function () {
    return view('giomar');
})->name('giomar');
