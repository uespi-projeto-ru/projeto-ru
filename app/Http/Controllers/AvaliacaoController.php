<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\avaliacao;
use App\cardapio;
use Carbon\Carbon;

class AvaliacaoController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

  public function index()
  {
      $avaliacao = avaliacao::all();

      $avaliacoes_unicas = array();
      foreach ($avaliacao as $aval){
        array_push($avaliacoes_unicas, ['data' => $aval->data, 'descricao' => $aval->descricao]);
      }

      $avaliacoes_unicas = array_unique($avaliacoes_unicas, SORT_REGULAR);
      $avaliacao = array();

      foreach($avaliacoes_unicas as $aval){
        $avaliacoes_equiv = avaliacao::where([['data', '=', $aval['data']], ['descricao', '=', $aval['descricao']]]);
        $quantidade = $avaliacoes_equiv->count();
        $nota_arroz = $avaliacoes_equiv->sum('nota_arroz') / $quantidade;
        $nota_feijao = $avaliacoes_equiv->sum('nota_feijao') / $quantidade;
        $nota_carne = $avaliacoes_equiv->sum('nota_carne') / $quantidade;
        $nota_suco = $avaliacoes_equiv->sum('nota_suco') / $quantidade;

        if($quantidade == 1){
          $nota_arroz = $avaliacoes_equiv->get()[0]->nota_arroz;
          $nota_feijao = $avaliacoes_equiv->get()[0]->nota_feijao;
          $nota_carne = $avaliacoes_equiv->get()[0]->nota_carne;
          $nota_suco = $avaliacoes_equiv->get()[0]->nota_suco;
        }

        array_push($avaliacao, ['data' => $aval['data'], 'descricao' => $aval['descricao'],
        'quantidade' => $quantidade, 'nota_arroz' => number_format($nota_arroz, 2),
        'nota_feijao' => number_format($nota_feijao, 2), 'nota_carne' => number_format($nota_carne, 2),
        'nota_suco' => number_format($nota_suco, 2)]);
      }

      return view('avaliacao.index',compact('avaliacao'))
          ->with('i', (request()->input('page', 1) - 1) * 5);
  }

  public function indexAluno()
  {
      $id = Auth::id();
      $avaliacao = avaliacao::where('id_aluno', $id)->where('pendente', False)->get();

      return view('avaliacao.avaliacoes',compact('avaliacao'))
          ->with('i', (request()->input('page', 1) - 1) * 5);
  }

  public function index_total()
  {
      $avaliacao = avaliacao::all();

      return view('avaliacao.index',compact('avaliacao'))
          ->with('i', (request()->input('page', 1) - 1) * 5);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('avaliacao.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      request()->validate([
          'descricao' => 'required',
          'data' => 'required',
      ]);

      avaliacao::create($request->all() + ['pendente' => True]);

      return redirect()->route('avaliacao.index')
          ->with('success', 'Avaliação criada com sucesso!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(avaliacao $avaliacao)
  {
      return view('avaliacao.show',compact('avaliacao'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(avaliacao $avaliacao)
  {
      return view('avaliacao.edit',compact('avaliacao'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, avaliacao $avaliacao)
  {
      #dd($request->all());
      request()->validate([
          'nota_arroz' => 'required|integer',
          'nota_feijao' => 'required|integer',
          'nota_carne' => 'required|integer',
          'nota_suco' => 'required|integer',
      ]);

      $avaliacao->update($request->all() + ['pendente' => False]);

      return redirect()->route('home')
                      ->with('success','Avaliação atualizada com sucesso!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(avaliacao $avaliacao)
  {
      $avaliacao->delete();

      return redirect()->route('avaliacao.index')
                      ->with('success','Avaliação deletada com sucesso!');
  }
}
