<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Pedido;
use App\ticket;
use App\User;
use App\Horario;
use App\cardapio;
use App\avaliacao;
use Carbon\Carbon;

class PedidoController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function indexPedidos(){
        $pedidos = DB::collection('pedidos')->where('user_id', Auth::id())->get()->sortBy('data_de_utilizacao');

        return view('alunoPedidos', compact('pedidos'));
    }

    public function compra(Ticket $ticket, Request $req){
        $horario = horario::all()->first();
        $inicio = Carbon::create($horario->inicio);
        $fim = Carbon::create($horario->fim);

        $idUser = Auth::id();
        $user = DB::table('users')->where('_id', $idUser)->first();
        $quantidadeNorm = $req->quantidade;
        if($quantidadeNorm == NULL){
          $quantidadeNorm = 1;
        }
        $quantidade = intval($quantidadeNorm);
        $saldoUser = (double) $user['saldo'];
        $ticketValor = (double) $ticket->valor;
        $ticketQuantidade = DB::table('pedidos')->where('user_id', Auth::id())->where('data_de_utilizacao', NULL)->get()->count();

        if ($ticket->estoque < $quantidade){
            return redirect()->route('home')->with('error', 'Estoque insuficiente');
        } else if(($ticketQuantidade + $quantidade) > $ticket->limite_compra){
            return redirect()->route('home')->with('error', 'Você está tentando comprar acima do limite de tickets.');
        } else if ($saldoUser < $ticketValor * $quantidade) {
            return redirect()->route('home')->with('error', 'Saldo insuficiente.');
        }
        else if($saldoUser >= $ticketValor) {
          for($i=0; $i < $quantidade; $i++){
            DB::collection('pedidos')->insert([
                'user_id' => $idUser,
                'ticket_id' => $ticket->id,
                'data_compra' => Carbon::now()->format('d/m/Y'),
                'descricao_cardapio' => NULL,
                'valor_ticket' => $ticketValor,
                'usado' => FALSE,
                'data_de_utilizacao' => NULL,
            ]);
          }
          User::where('_id', $idUser)->update(['saldo' => $user['saldo'] - ($ticketValor * $quantidade)]);
          ticket::where('_id', $ticket->id)->update(['estoque' => $ticket->estoque - $quantidade]);

          return redirect()->route('home')->with('success','Compra finalizada!');
        }
      }

    public function alunoSaldoAdd(Request $request){
      request()->validate([
        'saldoAdd' => 'required',
      ]);

      $user = DB::table('users')->where('_id', Auth::id())->first();

      (double) $saldoAdd = str_replace(",",".",$request->saldoAdd);

      User::where('_id', Auth::id())->update(['saldo' => $user['saldo'] + $saldoAdd]);

      return redirect()->route('home')->with('success', 'Saldo Adicionado!');
    }

    public function alunoUsarTicket(){
      $funcionando = FALSE;
      $horarios = DB::table('horarios')->get();

      foreach ($horarios as $horario){
        $data_inicio = Carbon::createFromFormat('d/m', $horario['inicioData']);
        $data_fim = Carbon::createFromFormat('d/m', $horario['fimData']);
        $hora_inicio = Carbon::createFromFormat('H:i', $horario['inicioHora']);
        $hora_fim = Carbon::createFromFormat('H:i', $horario['fimHora']);

        if($data_inicio->isPast() and $data_fim->isFuture()){
          if($hora_inicio->isPast() and $hora_fim->isFuture()){
            $funcionando = TRUE;
          }
        }
      }

      if($funcionando){
        $tickets = DB::table('pedidos')->where('user_id', Auth::id())->get();
        $ticketUsavel = FALSE;
        $dataDeHoje = Carbon::now()->format('d/m/Y');

        if($tickets->where('data_de_utilizacao', $dataDeHoje)->count() >= ticket::first()->limite_uso){
          return redirect()->route('home')->with('error', 'O limite de tickets usáveis diário já foi atingido.');
        }

        foreach ($tickets as $ticket){
          if ($ticket['data_de_utilizacao'] == NULL){
            $ticketUsavel = $ticket;
          }
        }

        if($ticketUsavel){
          $alimentoDeHoje = Cardapio::where('data', Carbon::now()->format('d/m/Y'))->first()->descricao;

          if($alimentoDeHoje == NULL) {
            $alimentoDeHoje = "Informação Indisponível.";
          }

          Pedido::where('_id', $ticketUsavel['_id'])->update(['data_de_utilizacao' => $dataDeHoje, 'descricao_cardapio' => $alimentoDeHoje]);
          if(avaliacao::where([['descricao', '=', $alimentoDeHoje], ['data' , '=' , $dataDeHoje]])->count() == 0){
            avaliacao::create(['id_aluno' => Auth::id(), 'descricao' => $alimentoDeHoje, 'pendente' => True, 'data' => $dataDeHoje]);
          }

          return redirect()->route('home')->with('success', 'O Ticket foi usado.');
        }
        else{
          return redirect()->route('home')->with('error', 'Nenhum ticket para ser usado.');
        }
      }
      else{
          return redirect()->route('home')->with('error', 'Fora do horário de funcionamento.');
      }
    }
}
