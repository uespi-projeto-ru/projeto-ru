<?php

namespace App\Http\Controllers;

use App\ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
       // $this->middleware('Auth');
    }

    public function index()
    {
        $ticket = ticket::all();

        return view('ticket.index',compact('ticket'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('ticket.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'limite_compra' => 'required',
            'limite_uso' => 'required',
            'valor' => 'required',
            'estoque' => 'required',
        ]);

        (float) $valor = str_replace(",", ".", $request->valor);

        ticket::create([
            "_token" => $request->_token,
            "estoque" => $request->estoque,
            "valor" => $valor,
            "limite_compra" => $request->limite_compra,
            "limite_uso" => $request->limite_uso,
        ]);

        return redirect()->route('ticket.index')
            ->with('success', 'Ticket criado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ticket $ticket)
    {
        return view('ticket.show',compact('ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ticket $ticket)
    {
        return view('ticket.edit',compact('ticket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ticket $ticket)
    {
        request()->validate([
            'valor' => 'required',
            'estoque' => 'required|integer|min:0',
            'limite_compra' => 'required|integer|min:0',
            'limite_uso' => 'required|integer|min:0',
        ]);

        $valor = doubleval(str_replace(",",".",$request->valor));
        $limite_compra = intval($request->limite_compra);
        $limite_uso = intval($request->limite_uso);

        $ticket->update([
            'valor' => $valor,
            'estoque' => $request->estoque,
            'limite_compra' => $limite_compra,
            'limite_uso' => $limite_uso,
        ]);

        return redirect()->route('ticket.index')
                        ->with('success','Ticket editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ticket $ticket)
    {
        $ticket->delete();


        return redirect()->route('ticket.index')
                        ->with('success','Ticket deletado com sucesso!');
    }
}
