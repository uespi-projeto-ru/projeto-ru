<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreatePedidosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::collection('pedidos')->delete();

        DB::collection('pedidos')->insert([
            'user_id' => (String) DB::collection('users')->where('is_admin', 0)->get()[0]['_id'],
            'ticket_id' => (String) DB::collection('cardapios')->get()[0]['_id'],
            'data_compra' => (String) DB::collection('cardapios')->get()[0]['data'],
            'descricao_cardapio' => DB::collection('cardapios')->get()[0]['descricao'],
            'valor_ticket' => 2.00,
            'usado' => TRUE,
            'data_de_utilizacao' => DB::collection('cardapios')->get()[0]['data'],
        ]);

        DB::collection('pedidos')->insert([
            'user_id' => (String) DB::collection('users')->where('is_admin', 0)->get()[0]['_id'],
            'ticket_id' => (String) DB::collection('cardapios')->get()[1]['_id'],
            'data_compra' => DB::collection('cardapios')->get()[1]['data'],
            'descricao_cardapio' => DB::collection('cardapios')->get()[1]['descricao'],
            'valor_ticket' => 2.00,
            'usado' => TRUE,
            'data_de_utilizacao' => DB::collection('cardapios')->get()[1]['data'],
    ]);

        DB::collection('pedidos')->insert([
            'user_id' => (String) DB::collection('users')->where('is_admin', 0)->get()[1]['_id'],
            'ticket_id' => (String) DB::collection('cardapios')->get()[1]['_id'],
            'data_compra' => DB::collection('cardapios')->get()[1]['data'],
            'descricao_cardapio' => DB::collection('cardapios')->get()[1]['descricao'],
            'valor_ticket' => 2.00,
            'usado' => TRUE,
            'data_de_utilizacao' => DB::collection('cardapios')->get()[1]['data'],
    ]);

        DB::collection('pedidos')->insert([
            'user_id' => (String) DB::collection('users')->where('is_admin', 0)->get()[2]['_id'],
            'ticket_id' => (String) DB::collection('cardapios')->get()[2]['_id'],
            'data_compra' => (String) DB::collection('cardapios')->get()[2]['data'],
            'descricao_cardapio' => DB::collection('cardapios')->get()[2]['descricao'],
            'valor_ticket' => 2.00,
            'usado' => TRUE,
            'data_de_utilizacao' => DB::collection('cardapios')->get()[2]['data'],
    ]);
    }
}
