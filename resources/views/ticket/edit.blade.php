@extends('layouts.app')

@section('content')
<center>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left" style="height: 20px; text-align: center">
                    <h2>Editar Ticket</h2>
                </div>
            </br>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('ticket.index') }}"> Voltar</a>
                </div>
            </div>
        </div>


        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        </br>
        <form action="{{ route('ticket.update',$ticket->id) }}" method="POST">
    	   @csrf
            @method('PUT')


            <div class="row" style="width: 80px; margin-right: 140px">
		      <div class="col-xs-12 col-sm-12 col-md-12" style="height: 0px">
		            <div class="form-group">
		                <strong>Valor:</strong>
                        <input class="form-control" style="height: 40px" name="valor" value="{{ $ticket->valor }}">
		          </div>
		      </div>
		      <div class="col-xs-12 col-sm-12 col-md-12" style="height: 0px; margin-left: 80px">
		            <div class="form-group">
		                <strong>Estoque:</strong>
                        <input class="form-control" style="height: 40px" name="estoque" value="{{ $ticket->estoque }}">
		          </div>
		      </div>
            <div class="col-xs-12 col-sm-12 col-md-12" style="margin-left: 160px">
		          <div class="form-group">
		                <strong>Limite de Compra:</strong>
                      <input class="form-control" style="height: 40px" name="limite_compra" value="{{ $ticket->limite_compra }}">
		          </div>
		      </div>
          <div class="col-xs-12 col-sm-12 col-md-12" style="margin-left: 160px">
            <div class="form-group">
                  <strong>Limite de Uso Diário:</strong>
                    <input class="form-control" style="height: 40px" name="limite_uso" value="{{ $ticket->limite_uso }}">
            </div>
        </div>
		  </div>
          <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Confirmar</button>
          </div>
        </form>
    </div>
</center>

@endsection
