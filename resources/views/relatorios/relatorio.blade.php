@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10 margin-tb">
            <div class="pull-left" style="margin-left: 10px; height: 10px">
                <h2>Relatórios</h2>
            </div>
        </br>
        </div>
    </div>


    </br>
    <center>
        <table id="tabela" style="width: 800px">
            <thead>
                <tr border-rules = none>
                    <th><input type="text" style="margin-bottom: 10px" placeholder="Filtrar Data" id="txtColuna1"/></th>
                </tr>
                </tr>
                <tr class="table table-bordered" >
                    <th>Data</th>
                    <th>Tickets Utilizados</th>
                    <th>Vendas</th>
                </tr>
            </thead>

            <tbody class="table table-bordered" >
                @foreach ($relatorios as $relatorio)
                <tr>
                    <td>
                    @if($relatorio['datas'] == NULL || $relatorio['datas'] == 0)

                    @else
                    {{ $relatorio['datas'] }}
                    @endif</td>

                    <td>
                    @if($relatorio['qtdCompras'] == NULL || $relatorio['qtdCompras'] == 0)

                    @else
                    {{ $relatorio['qtdCompras'] }}
                    @endif</td>

                    <td>
                        @if($relatorio['dinheiros'] == NULL || $relatorio['dinheiros'] == 0)

                        @else
                        R${{ number_format($relatorio['dinheiros'], 2, '.', '') }}
                        @endif</td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </center>

    <a class="btn btn-secondary" href="{{ route('admin.home') }}">Voltar</a>
</div>
@endsection
